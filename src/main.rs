use clap::{App, AppSettings, Arg};
use std::fs::File;
use std::io::{BufRead, BufReader, Result};

fn main() -> Result<()> {
    let matches = App::new("crat")
        .version("0.1")
        .author("Darrien, the new maintainer of coreutils :)")
        .about("cr(ust)at - rust cat")
        .setting(AppSettings::TrailingVarArg)
        .arg(
            Arg::with_name("FILE")
                .required(true)
                .multiple(true)
                .help("File to cat out"),
        )
        .get_matches();

    for file in matches.values_of("FILE").unwrap() {
        print_lines_in_file(File::open(file)?);
    }

    Ok(())
}

fn print_lines_in_file(file: File) {
    for line in BufReader::new(file).lines() {
        println!("{}", line.unwrap());
    }
}
